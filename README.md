# Generate randomize strings [A-Z0-9] with length 8  

### Install need go package
```
$: go get -u github.com/go-redis/redis
```

### Start single mode
```
$: docker-compose run app go run main.go
```
### Start multi mode
```
$: docker-compose run app go run rootine.go
```

## Locking result into Redis 
### Go into Redis
```
docker exec -it redis_app redis-cli
```
### Count of unique keys
```
info keyspace
``` 

### Lucking of unique keys
```
KEYS *
``` 

### Flush all DB data
```
FLUSHALL
``` 

