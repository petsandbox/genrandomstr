package main

import (
	"fmt"
	"github.com/go-redis/redis"
	"math/rand"
	"runtime"
	"time"
)

const (
	hashLen    = 8       // The length of random string
	chunk      = 50000   // The count of rows one time insert to Redis
	hashCount  = 3000000 // The need count of random string
	seedStrLen = 35      // The length of seedStr
	seedStr    = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	network    = "unix"
	addr       = "/tmp/redis.sock"
	password   = "" // no password set
	db         = 0  // use default DB

)

var clientNew *redis.Client

func init() {
	clientNew = redis.NewClient(&redis.Options{
		Network:  network,
		Addr:     addr,
		Password: password,
		DB:       db,
	})
}

func main() {
	numCPU := runtime.NumCPU()
	runtime.GOMAXPROCS(numCPU - 1)
	pingRedisRoot() //Test on connect to Redis

	fmt.Println("Number of processors: ", numCPU)
	start := time.Now().UTC().Unix()
	numWorkers := numCPU - 2

	c := make(chan []interface{})

	for ; numWorkers > 0; numWorkers-- {
		worker := &Worker{id: numWorkers}
		go worker.process(c)
	}

	randomStringChannel(c)
	close(c)
	sec := time.Now().UTC().Unix() - start
	fmt.Println("End: Redis insert count:", hashCount, " Sec:", sec)

}

func randomStringChannel(c chan []interface{}) {
	rand.Seed(time.Now().UTC().UnixNano())
	val := "" // Value for redis key
	hash := make([]uint8, hashLen)
	data := make([]interface{}, 0)

	for j := 1; j < hashCount+1; j++ {
		for i := 0; i < hashLen; i++ {
			hash[i] = seedStr[rand.Intn(seedStrLen)]
		}
		data = append(data, string(hash), val)
		if j%chunk == 0 {
			c <- data
			data = data[:0]
			//@TODO Implement select
		}
	}

	defer func(data []interface{}) {
		c = nil
	}(data)
}

type Worker struct {
	id int
}

func (w *Worker) process(c chan []interface{}) {
	data := make([]interface{}, 0)

	//@TODO Change to busy status

	for {
		data = <-c
		RedisSetDataRoot(data)
		data = data[:0]
	}

	//@TODO Create defer
}

func RedisSetDataRoot(data []interface{}) {
	err := clientNew.MSet(data...).Err()
	if err != nil {
		panic(err)
	}
}

func pingRedisRoot() {
	pong, err := clientNew.Ping().Result()
	fmt.Println(pong, err)
	if err != nil {
		panic(err)
	}
}
