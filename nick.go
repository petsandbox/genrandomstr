package main

import (
	"fmt"
	"math/rand"
	"time"
)

const (
	hashLen    = 8       // The length of random string
	hashCount  = 1000000 // The need count of random string
	seedStrLen = 36      // The length of seedStr
	seedStr    = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
)


func main() {

	start := time.Now().UTC().Unix()
	fmt.Println("Start:")

	data := randomString() // To generate a random strings

	// Work with strings
	/*
	for k, _ := range data {
		//@TODO something
		fmt.Println(k)
	}
	*/

	sec := time.Now().UTC().Unix() - start
	len := len(data)
	col := hashCount - len
	fmt.Println("Generate the strings count:", hashCount, "\nUnique have generate count:", len , "\nCollision count:", col,  "\nSpend time: sec:", sec)
}



func randomString() map[string]string {
	rand.Seed(time.Now().UTC().UnixNano())
	val := "" // Value for redis key
	hash := make([]uint8, hashLen)
	
	data := make(map[string]string)
	for j := 0; j < hashCount; j++ {
		for i := 0; i < hashLen; i++ {
			hash[i] = seedStr[rand.Intn(seedStrLen)]
		}
		data[string(hash)] = val		
	}

	return data
}

