package main

import (
	"fmt"
	"github.com/go-redis/redis"
	"math/rand"
	"time"
)

const (
	hashLen    = 8       // The length of random string
	chunk      = 100000  // The count of rows one time insert to Redis
	hashCount  = 3000000 // The need count of random string
	seedStrLen = 35      // The length of seedStr
	seedStr    = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	network    = "unix"
	addr       = "/tmp/redis.sock"
	password   = "" // no password set
	db         = 0  // use default DB
)

var client *redis.Client

func init() {
	client = redis.NewClient(&redis.Options{
		Network:  network,
		Addr:     addr,
		Password: password,
		DB:       db,
	})
}

func main() {

	pingRedis() //Test on connect to Redis

	start := time.Now().UTC().Unix()
	fmt.Println("Start:")

	data := randomString() // To generate a random strings

	sec := time.Now().UTC().Unix() - start
	fmt.Println("Generated a hash count:", hashCount, " Sec:", sec)

	RedisSetData(data) //Insert to Redis

	sec = time.Now().UTC().Unix() - start
	fmt.Println("End: Redis insert count:", hashCount, " Sec:", sec)
}


func randomString() []interface{} {
	rand.Seed(time.Now().UTC().UnixNano())
	val := "" // Value for redis key
	hash := make([]uint8, hashLen)
	data := make([]interface{}, 0)

	for j := 0; j < hashCount; j++ {
		for i := 0; i < hashLen; i++ {
			hash[i] = seedStr[rand.Intn(seedStrLen)]
		}
		data = append(data, string(hash), val)
	}
	return data
}

func RedisSetData(data []interface{}) {
	cnt := 2 * hashCount / chunk
	for i := 0; i < cnt; i++ {
		m := i * chunk
		n := m + chunk
		err := client.MSet(data[m:n]...).Err()
		if err != nil {
			panic(err)
		}
	}
}

func pingRedis() {
	pong, err := client.Ping().Result()
	fmt.Println(pong, err)
	if err != nil {
		panic(err)
	}
}
